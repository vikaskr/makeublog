<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $post_title
 * @property integer $post_date
 * @property string $post_excerpt
 * @property string $status
 * @property string $url
 * @property string $post_name
 * @property string $post_brief
 * @property integer $post_parent
 * @property string $pinned
 * @property integer $order
 * @property string $post_type
 * @property integer $comment_count
 * @property integer $approved_by
 * @property string $approved_on
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Post $postParent
 * @property Post[] $posts
 * @property PostCategory[] $postCategories
 * @property PostComment[] $postComments
 * @property PostDetail[] $postDetails
 * @property PostPingStats[] $postPingStats
 * @property PostTags[] $postTags
 */
class Post extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'post_date','post_parent', 'order', 'comment_count', 'approved_by'], 'integer'],
            [['post_title','post_type'], 'required'],
            [['status', 'post_brief', 'pinned', 'del_status'], 'string'],
            [['approved_on', 'created_at', 'updated_at'], 'safe'],
            [['post_title', 'post_excerpt', 'url', 'post_name'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['post_parent'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_parent' => 'id']],
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['create_post'] = ['post_title'];
    //     $scenarios['update_post'] = ['post_title'];
    //     return $scenarios;
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'post_title' => Yii::t('app', 'Post Title'),
            'post_date' => Yii::t('app', 'Post Date'),
            'post_excerpt' => Yii::t('app', 'Post Excerpt'),
            'status' => Yii::t('app', 'Status'),
            'url' => Yii::t('app', 'Url'),
            'post_name' => Yii::t('app', 'Post Name'),
            'post_brief' => Yii::t('app', 'Post Brief'),
            'post_parent' => Yii::t('app', 'Post Parent'),
            'pinned' => Yii::t('app', 'Pinned'),
            'order' => Yii::t('app', 'Order'),
            'post_type' => Yii::t('app', 'Post Type'),
            'comment_count' => Yii::t('app', 'Comment Count'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'approved_on' => Yii::t('app', 'Approved On'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostParent()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_parent'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['post_parent' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostCategory()
    {
        return $this->hasOne(PostCategory::className(), ['post_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostComments()
    {
        return $this->hasMany(PostComment::className(), ['post_id' => 'id'])->where(['del_status' => '0'])->andWhere(['<>','approved_by','NULL']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostDetail()
    {
        return $this->hasOne(PostDetail::className(), ['post_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostPingStats()
    {
        return $this->hasMany(PostPingStats::className(), ['post_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTags::className(), ['post_id' => 'id'])->where(['del_status' => '0']);
    }

    public function getPostImages()
    {
       return $this->hasMany(PostImages::className(), ['post_id' => 'id']);
    }
}
