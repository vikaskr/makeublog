<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_element".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $type
 * @property string $tag
 * @property string $parent_tag
 * @property string $parent_class
 * @property string $class
 * @property integer $order
 * @property string $x_start
 * @property string $y_start
 * @property string $x_end
 * @property string $y_end
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property PageElements[] $pageElements
 */
class MubElement extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['name', 'title', 'type', 'tag', 'parent_tag', 'parent_class', 'class', 'x_start', 'y_start', 'x_end', 'y_end'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'tag' => Yii::t('app', 'Tag'),
            'parent_tag' => Yii::t('app', 'Parent Tag'),
            'parent_class' => Yii::t('app', 'Parent Class'),
            'class' => Yii::t('app', 'Class'),
            'order' => Yii::t('app', 'Order'),
            'x_start' => Yii::t('app', 'X Start'),
            'y_start' => Yii::t('app', 'Y Start'),
            'x_end' => Yii::t('app', 'X End'),
            'y_end' => Yii::t('app', 'Y End'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageElements()
    {
        return $this->hasMany(PageElements::className(), ['element_id' => 'id'])->where(['del_status' => '0']);
    }
}
