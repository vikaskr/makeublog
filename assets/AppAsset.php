<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/normalize.css',
        '/css/main.css',
        '/css/bootstrap.min.css',
        '/css/animate.min.css',
        '/css/font-awesome.min.css',
        '/vendor/OwlCarousel/owl.carousel.min.css',
        '/vendor/OwlCarousel/owl.theme.default.min.css',
        '/vendor/slider/css/preview.css',
        '/vendor/slider/css/nivo-slider.css',
        '/css/meanmenu.min.css',
        '/css/magnific-popup.css',
        '/css/hover-min.css',
        '/css/style.css',
    ];
    public $js = [
        '/js/modernizr-2.8.3.min.js',
        '/js/jquery-2.2.4.min.js',
        '/js/plugins.js',
        '/js/popper.js',
        '/js/bootstrap.min.js',
        '/js/wow.min.js',
        '/vendor/OwlCarousel/owl.carousel.min.js',
        '/vendor/slider/js/jquery.nivo.slider.js',
        '/js/jquery.meanmenu.min.js',
        '/js/jquery.scrollUp.min.js',
        '/js/jquery.counterup.min.js',
        '/js/waypoints.min.js',
        '/js/isotope.pkgd.min.js',
        '/js/jquery.magnific-popup.min.js',
        '/js/ticker.js',
        '/js/main.js',
        '/js/custom_mub_frontend.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
       
        'app\assets\FontAwesomeAsset'
    ];
}
