<?php 
namespace app\helpers;
use yii\data\Pagination;

class SearchHelper
{
	public static function searchString($model,$attrib,$searchString)
	{
		$where = ['del_status' => '0'];
		$query = $model::find()->where($where)->andWhere(['REGEXP', $attrib,$searchString]);
		// p($query->createCommand()->rawSql);
		$pages = new Pagination(['totalCount' => $query->count(),'pageSize' => 10]);
        $allPosts = $query->offset($pages->offset)->limit($pages->limit)->all();
        $data = ['searchResult' => $allPosts,'pages' => $pages];
	    return $data;
	}
}
