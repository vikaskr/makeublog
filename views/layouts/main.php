<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\MubCategory;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!doctype html>
<html class="no-js" lang="">
<head>
<title>Home :: MakeUBig | Business Blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<?= Html::csrfMetaTags() ?>
<?php $this->head();?>

</head>
<body>
<?php $this->beginBody() ?>
        <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an 
        <strong>outdated</strong> browser. Please 
        <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
    </p>
    <![endif]-->
        <!-- Add your site or application content here -->
        <!-- Preloader Start Here -->
        <div id="preloader"></div>
        <!-- Preloader End Here -->
        <div id="wrapper" class="wrapper">
            <!-- Header Area Start Here -->
            <header>
                <div id="header-layout1" class="header-style1">
                    <div class="main-menu-area bg-primarytextcolor header-menu-fixed" id="sticker">
                        <div class="container">
                            <div class="row no-gutters d-flex align-items-center">
                                <div class="col-lg-2 d-none d-lg-block">
                                    <div class="logo-area">
                                        <a href="/">
                                            <img src="/img/logo.png" alt="logo" class="img-fluid">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xl-8 col-lg-7 position-static min-height-none">
                                    <div class="ne-main-menu">
                                        <nav id="dropdown">
                                            <ul>
                                                <li class="active">
                                                    <a href="index.html#">Home</a>
                                                    <ul class="ne-dropdown-menu">
                                                        <li class="active">
                                                            <a href="index.html">Home 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="index2.html">Home 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="index3.html">Home 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="index4.html">Home 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="index5.html">Home 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="index6.html">Home 6</a>
                                                        </li>
                                                        <li>
                                                            <a href="index7.html">Home 7</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="index.html#">Post</a>
                                                    <ul class="ne-dropdown-menu">
                                                        <li>
                                                            <a href="post-style-1.html">Post Style 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="post-style-2.html">Post Style 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="post-style-3.html">Post Style 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="post-style-4.html">Post Style 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="single-news-1.html">News Details 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="single-news-2.html">News Details 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="single-news-3.html">News Details 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="index.html#">Pages</a>
                                                    <ul class="ne-dropdown-menu">
                                                        <li>
                                                            <a href="author-post.html">Author Post Page</a>
                                                        </li>
                                                        <li>
                                                            <a href="archive.html">Archive Page</a>
                                                        </li>
                                                        <li>
                                                            <a href="gallery-style-1.html">Gallery Style 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="gallery-style-2.html">Gallery Style 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="404.html">404 Error Page</a>
                                                        </li>
                                                        <li>
                                                            <a href="contact.html">Contact Page</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="post-style-1.html">Politics</a>
                                                </li>
                                                <li>
                                                    <a href="post-style-2.html">Business</a>
                                                </li>
                                                <li>
                                                    <a href="post-style-3.html">Sports</a>
                                                </li>
                                                <li>
                                                    <a href="post-style-4.html">Fashion</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-lg-3 col-md-12 text-right position-static">
                                    <div class="header-action-item">
                                        <ul>
                                            <li>
                                                <form id="top-search-form" class="header-search-light">
                                                    <input type="text" class="search-input" placeholder="Search...." required="" style="display: none;">
                                                    <button class="search-button">
                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            </li>
                                            <li>
                                                <button type="button" class="login-btn" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-user" aria-hidden="true"></i>Sign in
                                                </button>
                                            </li>
                                            <li>
                                                <div id="side-menu-trigger" class="offcanvas-menu-btn">
                                                    <a href="index.html#" class="menu-bar">
                                                        <span></span>
                                                        <span></span>
                                                        <span></span>
                                                    </a>
                                                    <a href="index.html#" class="menu-times close">
                                                        <span></span>
                                                        <span></span>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- Header Area End Here -->
            <!-- News Feed Area Start Here -->
            <section class="bg-accent border-bottom add-top-margin">
                <div class="container">
                    <div class="row no-gutters d-flex align-items-center">
                        <div class="col-lg-2 col-md-3 col-sm-4 col-6">
                            <div class="topic-box topic-box-margin">Top Stories</div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-8 col-6">
                            <div class="feeding-text-dark">
                                <ol id="sample" class="ticker">
                                    <li>
                                        <a href="index.html#">McDonell Kanye West highlights difficulties for celebritiesComplimentary decor and
                                            design advicewith Summit Park homes</a>
                                    </li>
                                    <li>
                                        <a href="index.html#">Magnificent Image Of The New Hoover Dam Bridge Taking Shape</a>
                                    </li>
                                    <li>
                                        <a href="index.html#">If Obama Had Governed Like This in 2017 He'd Be the Transformational.</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- News Feed Area End Here -->
            <!-- News Info List Area Start Here -->
            <section class="bg-body">
                <div class="container">
                    <ul class="news-info-list text-center--md">
                        <li>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>India</li>
                        <li>                                
                            <i class="fa fa-calendar" aria-hidden="true"></i><span id="current_date"></span></li>
                        <li>
                            <i class="fa fa-clock-o" aria-hidden="true"></i>Last Update 11.30 am</li>
                    </ul>
                </div>
            </section></div>
    <?= $content ?>
<footer>
                <div class="footer-area-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="footer-box">
                                    <h2 class="title-bold-light title-bar-left text-uppercase">Most Viewed Posts</h2>
                                    <ul class="most-view-post">
                                        <li>
                                            <div class="media">
                                                <a href="post-style-1.html">
                                                    <img src="/img/footer/post1.jpg" alt="post" class="img-fluid">
                                                </a>
                                                <div class="media-body">
                                                    <h3 class="title-medium-light size-md mb-10">
                                                        <a href="index.html#">Basketball Stars Face Off itim ate Playoff Beard Battle</a>
                                                    </h3>
                                                    <div class="post-date-light">
                                                        <ul>
                                                            <li>
                                                                <span>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span>November 11, 2017</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <a href="post-style-2.html">
                                                    <img src="/img/footer/post2.jpg" alt="post" class="img-fluid">
                                                </a>
                                                <div class="media-body">
                                                    <h3 class="title-medium-light size-md mb-10">
                                                        <a href="index.html#">Basketball Stars Face Off in ate Playoff Beard Battle</a>
                                                    </h3>
                                                    <div class="post-date-light">
                                                        <ul>
                                                            <li>
                                                                <span>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span>August 22, 2017</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <a href="post-style-3.html">
                                                    <img src="/img/footer/post3.jpg" alt="post" class="img-fluid">
                                                </a>
                                                <div class="media-body">
                                                    <h3 class="title-medium-light size-md mb-10">
                                                        <a href="index.html#">Basketball Stars Face tim ate Playoff Battle</a>
                                                    </h3>
                                                    <div class="post-date-light">
                                                        <ul>
                                                            <li>
                                                                <span>
                                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                </span>March 31, 2017</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-3 col-md-6 col-sm-12">
                                <div class="footer-box">
                                    <h2 class="title-bold-light title-bar-left text-uppercase">Popular Categories</h2>
                                    <ul class="popular-categories">
                                        <li>
                                            <a href="index.html#">Gadgets
                                                <span>15</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">Architecture
                                                <span>10</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">New look 2017
                                                <span>14</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">Reviews
                                                <span>13</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">Mobile and Phones
                                                <span>19</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">Recipes
                                                <span>26</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">Decorating
                                                <span>21</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index.html#">IStreet fashion
                                                <span>09</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-5 col-md-12 col-sm-12">
                                <div class="footer-box">
                                    <h2 class="title-bold-light title-bar-left text-uppercase">Post Gallery</h2>
                                    <ul class="post-gallery shine-hover ">
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style1.html">
                                                <figure>
                                                    <img src="/img/footer/post4.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style2.html">
                                                <figure>
                                                    <img src="/img/footer/post5.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style1.html">
                                                <figure>
                                                    <img src="/img/footer/post6.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style2.html">
                                                <figure>
                                                    <img src="/img/footer/post7.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style1.html">
                                                <figure>
                                                    <img src="/img/footer/post8.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style2.html">
                                                <figure>
                                                    <img src="/img/footer/post9.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style1.html">
                                                <figure>
                                                    <img src="/img/footer/post10.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style2.html">
                                                <figure>
                                                    <img src="/img/footer/post11.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/gallery-style1.html">
                                                <figure>
                                                    <img src="/img/footer/post12.jpg" alt="post" class="img-fluid">
                                                </figure>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-area-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="index.html" class="footer-logo img-fluid">
                                    <img src="/img/logo.png" alt="logo" class="img-fluid">
                                </a>
                                <ul class="footer-social">
                                    <li>
                                        <a href="index.html#" title="facebook">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="twitter">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="google-plus">
                                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="linkedin">
                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="pinterest">
                                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="rss">
                                            <i class="fa fa-rss" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="index.html#" title="vimeo">
                                            <i class="fa fa-vimeo" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                                <p>© 2017 newsedge Designed by RadiusTheme. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Footer Area End Here -->
            <!-- Modal Start-->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="title-login-form">Login</div>
                        </div>
                        <div class="modal-body">
                            <div class="login-form">
                                <form>
                                    <label>Username or email address *</label>
                                    <input type="text" placeholder="Name or E-mail" />
                                    <label>Password *</label>
                                    <input type="password" placeholder="Password" />
                                    <div class="checkbox checkbox-primary">
                                        <input id="checkbox" type="checkbox" checked>
                                        <label for="checkbox">Remember Me</label>
                                    </div>
                                    <button type="submit" value="Login">Login</button>
                                    <button class="form-cancel" type="submit" value="">Cancel</button>
                                    <label class="lost-password">
                                        <a href="index.html#">Lost your password?</a>
                                    </label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal End-->
            <!-- Offcanvas Menu Start -->
            <div id="offcanvas-body-wrapper" class="offcanvas-body-wrapper">
                <div id="offcanvas-nav-close" class="offcanvas-nav-close offcanvas-menu-btn">
                    <a href="index.html#" class="menu-times re-point">
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <div class="offcanvas-main-body">
                    <ul id="accordion" class="offcanvas-nav panel-group">
                        <li class="panel panel-default">
                            <div class="panel-heading">
                                <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseOne">
                                    <i class="fa fa-home" aria-hidden="true"></i>Home Pages</a>
                            </div>
                            <div aria-expanded="false" id="collapseOne" role="tabpanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="offcanvas-sub-nav">
                                        <li>
                                            <a href="index.html">Home 1</a>
                                        </li>
                                        <li>
                                            <a href="index2.html">Home 2</a>
                                        </li>
                                        <li>
                                            <a href="index3.html">Home 3</a>
                                        </li>
                                        <li>
                                            <a href="index4.html">Home 4</a>
                                        </li>
                                        <li>
                                            <a href="index5.html">Home 5</a>
                                        </li>
                                        <li>
                                            <a href="index6.html">Home 6</a>
                                        </li>
                                        <li>
                                            <a href="index7.html">Home 7</a>
                                        </li>
                                        <li>
                                            <a href="https://www.radiustheme.com/demo/html/newsedge/newsedge/index8.html">Home 8</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="author-post.html">
                                <i class="fa fa-user" aria-hidden="true"></i>Author Post Page</a>
                        </li>
                        <li class="panel panel-default">
                            <div class="panel-heading">
                                <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseTwo">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>Post Pages</a>
                            </div>
                            <div aria-expanded="false" id="collapseTwo" role="tabpanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="offcanvas-sub-nav">
                                        <li>
                                            <a href="post-style-1.html">Post Style 1</a>
                                        </li>
                                        <li>
                                            <a href="post-style-2.html">Post Style 2</a>
                                        </li>
                                        <li>
                                            <a href="post-style-3.html">Post Style 3</a>
                                        </li>
                                        <li>
                                            <a href="post-style-4.html">Post Style 4</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="panel panel-default">
                            <div class="panel-heading">
                                <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseThree">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>News Details Pages</a>
                            </div>
                            <div aria-expanded="false" id="collapseThree" role="tabpanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="offcanvas-sub-nav">
                                        <li>
                                            <a href="single-news-1.html">News Details 1</a>
                                        </li>
                                        <li>
                                            <a href="single-news-2.html">News Details 2</a>
                                        </li>
                                        <li>
                                            <a href="single-news-3.html">News Details 3</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="archive.html">
                                <i class="fa fa-archive" aria-hidden="true"></i>Archive Page</a>
                        </li>
                        <li class="panel panel-default">
                            <div class="panel-heading">
                                <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="index.html#collapseFour">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>Gallery Pages</a>
                            </div>
                            <div aria-expanded="false" id="collapseFour" role="tabpanel" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="offcanvas-sub-nav">
                                        <li>
                                            <a href="gallery-style-1.html">Gallery Style 1</a>
                                        </li>
                                        <li>
                                            <a href="gallery-style-2.html">Gallery Style 2</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="404.html">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>404 Error Page</a>
                        </li>
                        <li>
                            <a href="contact.html">
                                <i class="fa fa-phone" aria-hidden="true"></i>Contact Page</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Offcanvas Menu End -->
      
<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>
