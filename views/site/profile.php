<?php 
use yii\bootstrap\ActiveForm;
$mubUserId = \app\models\User::getMubUserId();
$mubUserModel = new \app\models\MubUser();
$currentUser = $mubUserModel::findOne($mubUserId);
$userDetails = $currentUser;
$contactDetails = $currentUser->mubUserContacts;
?>

<div class="container">
     
      <div class="agent-grids">
      <div class="col-md-12 agent-grid" style="margin-top: 3em;">
         <div class="col-md-5 agent-right"> 
           <img src="/images/1.jpg" height="360" width="460">
         </div>
         <div class="col-md-6 agent-right">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','options' => ['method' => 'POST'],'action' => ['/site/profile']]); ?>
            <?= $form->field($currentUser, 'first_name')->textInput(['autofocus' => true,'class' => 'form-control']);?>
           <?= $form->field($currentUser, 'last_name')->textInput(['class' => 'form-control']);?>
            <?= $form->field($currentUser, 'username')->textInput(['class' => 'form-control','readonly' => true]);?>
            <?= $form->field($currentUser, 'password')->passwordInput(['class' => 'form-control']);?>
            <?= $form->field($contactDetails, 'mobile')->textInput(['class' => 'form-control']);?>
             <?= $form->field($contactDetails, 'email')->textInput(['class' => 'form-control']);?>
             <?= $form->field($currentUser,'id')->hiddenInput()->label(false);?>
           <center><input type="submit" class="btn btn-primary show" value="Update" style="margin-bottom: 2em; margin-left: 0px!important;"></center>
           <?php ActiveForm::end(); ?>
         </div>
         <div class="clearfix"></div>
      </div>
      </div>
</div>