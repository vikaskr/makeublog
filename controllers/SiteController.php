<?php

namespace app\controllers;

use app\components\GenericController;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;

class SiteController extends GenericController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->view->params['page'] = 'home';
        $signupMail = new \app\models\SignupMail();
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $postImages = new \app\models\PostImages();
        $mubCategory = new \app\models\MubCategory();
        $postCategory = new \app\models\PostCategory();
        $blog = $postModel::find()->where(['del_status' => '0'])->all();
        
        $blogs = "select `post`.`id`,`post`.`mub_user_id`,`post`.`post_title`,`post`.`created_at`,`post`.`post_type`,`post`.`post_excerpt`,`post`.`url`,`post_category`.`post_id`,`post_category`.`category_id` from `post` INNER JOIN `post_category` on `post`.`id` = `post_category`.`post_id` where `post_category`.`del_status` = '0' AND `post`.`del_status` = '0' AND `post`.`post_type` = 'featured' AND `post`.`status` = 'active' ORDER BY `post`.`id`";

        $blogsCat = "select `post`.`id`,`post`.`mub_user_id`,`post`.`post_title`,`post`.`created_at`,`post`.`post_type`,`post`.`post_excerpt`,`post`.`url`,`post_category`.`post_id`,`post_category`.`category_id` from `post` INNER JOIN `post_category` on `post`.`id` = `post_category`.`post_id` where `post_category`.`del_status` = '0' AND `post`.`del_status` = '0' AND `post`.`post_type` = 'featured' AND `post`.`status` = 'active' ORDER BY `post`.`id` LIMIT 5";

        $allBlogs = \yii::$app->db->createCommand($blogs)->queryAll(); 
        $catBlogs = \yii::$app->db->createCommand($blogsCat)->queryAll(); 
        return $this->render('index',['allBlogs' => $allBlogs,'postImages' => $postImages,'mubUserModel' => $mubUserModel,'mubCategory'=> $mubCategory,'postCategory' => $postCategory,'catBlogs' => $catBlogs]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack('/mub-admin/');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
   
    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $this->view->params['page'] = 'contact';
        $model = new ContactForm();
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $postImages = new \app\models\PostImages();
        $contactMail = new \app\models\ContactMail();
        $params = \Yii::$app->request->post();
        if(!empty($params) && $contactMail->load($params))
        {
            $this->layout = false;
            if($contactMail->save(false))
            {
                \Yii::$app->mailer->compose()
                ->setTo('mk@inversiosolutions.com')
                ->setBcc('admin@makeubig.com')
                ->setFrom(['admin@techinversio.com' => 'TechInversio'])
                ->setSubject('Someone wants to connect with You')
                ->setHtmlBody("<b>The following are the details of the Interested Client</b><br/><br/>
            <b>Email Id : </b>'.$contactMail->email.'<br /><b>Request Message : </b>'.
            $contactMail->message.'<br /><b>")
                ->send();    
                return true;
            }
            else
            {
                p($contactMail->getErrors());
            }
                    
        }
        return $this->render('contact', [
            'postModel' => $postModel,
            'contactMail' => $contactMail,
        ]);

    }

    public function actionSignup()
    {
        $params = \yii::$app->request->getBodyParams();
        $signupMail = new \app\models\SignupMail();
        if($signupMail->load($params))
         {
          if ($signupMail->save())
          {
             \Yii::$app->mailer->compose()
                ->setTo('mk@inversiosolutions.com')
                ->setBcc('admin@makeubig.com')
                ->setFrom(['admin@techinversio.com' => 'TechInversio'])
                ->setSubject('Customer Signed Up')
                ->setHtmlBody("<b>A client with Mail Id ".$signupMail->email." Signed up to your Newsletter")
                ->send();    
            return true;
            }
            else
            {
            return false;
            }
        }
    }

    public function actionRegister()
    {
        $this->layout = 'admin';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) 
        {
            if ($user = $model->signup()) 
            {
                if (Yii::$app->getUser()->login($user))
                {
                    return $this->goBack('/mub-admin');
                }
            }
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        $this->view->params['page'] = 'about';
        return $this->render('about');
    }

    public function actionProfile()
    {   
        $this->view->params['page'] = 'about';
        if (Yii::$app->user->isGuest) {
            $this->redirect('/');
        }
        else 
        {
            if(Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $mubUserContact = $mubUser->mubUserContacts;
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                         return $this->goBack('/site/profile');
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

    public function actionPostDetails($name)
    {
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $post = $postModel::find()->where(['url' => $name,'del_status' => '0'])->one();
        $mubUser = $mubUserModel::findOne($post->mub_user_id);
        $postImages = new \app\models\PostImages();
        $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
        $postDetail = $post->postDetail;
        $currentReads = intval($postDetail->read_count);
        $currentReads++;
        $postDetail->read_count = $currentReads;
        if(!$postDetail->save())
        {
            p($postDeatil->getErrors());
        }
        $comments['comments'] = $post->postComments;
        $comments['comments_count'] = count($post->postComments);
        $this->view->params['page'] = 'blogs';   
        return $this->render('post-details',[
            'post' => $post,
            'postDetail' => $postDetail,
            'mubUser' => $mubUser,
            'postImage' => $postImage,
            'comments' => $comments
        ]);
    }
}
