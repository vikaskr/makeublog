<?php

namespace app\controllers;
use yii\data\Pagination;
use app\components\GenericController;
use yii\helpers\ArrayHelper;

class BlogController extends GenericController
{
    public function actionIndex($category=null)
    {
    	$this->view->params['page'] = 'blogs';
        $signupMail = new \app\models\SignupMail();
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $postImages = new \app\models\PostImages();
        $postComment = new \app\models\PostComment();
        $query = $postModel::find()->andWhere(['del_status' => '0','status' => 'active'])->orderBy(['id' => SORT_DESC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize' => 10]);
        $allPosts = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index',['postModel' => $postModel,'mubUserModel' => $mubUserModel,'postImages' => $postImages,'postComment' => $postComment,'signupMail' => $signupMail, 'allPosts' => $allPosts,'pages' => $pages]);
    }

    public function actionSearch($s)
    {
        $this->view->params['page'] = 'blogs';
    	if($s != null)
    	{
            $postDetailModel = new \app\models\PostDetail();
    	    $searchResult = \app\helpers\SearchHelper::searchString($postDetailModel,'post_content',$s);
            return $this->render('search',['blogs' => $searchResult['searchResult'],'pages' => $searchResult['pages']]);
    	}
	   	throw new \yii\web\HttpException(400, 'This is not a valid request');
    }

    public function actionCategory($name)
    {
        $this->view->params['page'] = 'blogs';
        $name = strtolower($name);
        if($name != null)
        {
            $categoryModel = new \app\models\MubCategory();
            $postCategory = new \app\models\PostCategory();
            $postDetail = new \app\models\PostDetail();
            $allPosts = [];
            if($name != 'all')
            {
                $category = $categoryModel::find()->where(['category_slug' => $name,'del_status' => '0'])->one();
                if(!empty($category))
                {
                    $postIds = $postCategory::find()->select(['post_id'])->where(['category_id' => $category->id,'del_status' => '0'])->all();
                    $ids =[];
                    foreach ($postIds as $postKey => $id){
                        $ids[] = $id->post_id;
                    }
                    $query = $postDetail::find()->where(['del_status' => '0','post_id' => $ids])->orderBy(['read_count' => SORT_DESC]);
                }
            }
            else
            {
                $category = $categoryModel::find()->where(['del_status' => '0'])->all();
                if(!empty($category))
                {
                    foreach ($category as $cat) 
                    {
                        $postIds = $postCategory::find()->select(['post_id'])->where(['category_id' => $cat->id,'del_status' => '0'])->all();
                        $ids =[];
                        foreach ($postIds as $postKey => $id){
                            $ids[] = $id->post_id;
                        }
                    }
                    $query = $postDetail::find()->where(['del_status' => '0','post_id' => $ids])->orderBy(['read_count' => SORT_DESC]);
                }
            }
            if(isset($query))
            {
                $pages = new Pagination(['totalCount' => $query->count(),'pageSize' => 10]);
                $allPosts = $query->offset($pages->offset)->limit($pages->limit)->all();
                return $this->render('search',['blogs' => $allPosts,'pages' => $pages]);
            }
            $pages = new Pagination(['totalCount' => 0,'pageSize' => 10]);
           return $this->render('search',['blogs' => [],'pages' => $pages]);
        }
        throw new \yii\web\HttpException(400, 'This is not a valid request');
    }

      

    public function actionPostDetail($id)
    {
        $postModel = new \app\models\Post();
        $mubUserModel = new \app\models\MubUser();
        $post = $postModel::find()->where(['url' => $id,'del_status' => '0'])->one();
        $mubUser = $mubUserModel::findOne($post->mub_user_id);
        $postImages = new \app\models\PostImages();
        $postImage = $postImages::find()->where(['post_id' => $post->id])->one();
        $postDetail = $post->postDetail;
        $currentReads = intval($postDetail->read_count);
        $currentReads++;
        $postDetail->read_count = $currentReads;
        if(!$postDetail->save())
        {
            p($postDeatil->getErrors());
        }
        $comments['comments'] = $post->postComments;
        $comments['comments_count'] = count($post->postComments);
        $this->view->params['page'] = 'blogs';   
        return $this->render('postdetail',[
            'post' => $post,
            'postDetail' => $postDetail,
            'mubUser' => $mubUser,
            'postImage' => $postImage,
            'comments' => $comments
        ]);
    }

    public function actionAddComment()
    {
        if (\Yii::$app->request->isAjax) 
        {
            $params = \Yii::$app->request->getBodyParams();
            if(!empty($params['PostComment']['url'])){
                $postUrl = $params['PostComment']['url'];
                $postsModel = new \app\models\Post();
                $post = $postsModel::find()->where(['del_status'=>'0','url' => $postUrl])->one();
                $postComments = new \app\models\PostComment();
                $postComments->load(\Yii::$app->request->getBodyParams());
                $postComments->post_id = $post->id;
                if($postComments->save())
                {
                   return $postComments->id; 
                }
            }
            return false;
        }
        else
        {
            throw new \yii\web\HttpException(400, 'This path is wrongly accessed');
        }
    }
}
