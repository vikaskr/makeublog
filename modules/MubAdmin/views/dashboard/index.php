<div class="x_panel">
  <div class="x_title">
    <h2>Your Dashboard</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \app\models\MubUser;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1>Blogs</h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 

    ?>

    <?php $columns = [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'mub_user_id',
            'post_title',
            'created_at',
            'updated_at'];
            $mubUserId = \app\models\User::getMubUserId();
            if($mubUserId=='1'){
            $columns[] = ['label' =>'Uploaded By',
            'attribute' => 'username',
            'value' => 'mubUser.username'];
            }
            // 'post_name',
            // 'post_brief:ntext',
            // 'post_parent',
            // 'pinned',
            // 'order',
            // 'post_type',
            // 'comment_count',
            // 'approved_by',
            // 'approved_on',
            if($mubUserId=='1'){
            $columns[] = [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'active';
                    $id = $model->id;
                    $valInactive = 'inactive';
                    return ($model->status == 'inactive') ? '<button class="btn btn-primary" onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valActive.'\''.','.'\''.$id.'\''.')">Activate</button>' : '<button class="btn btn-danger" onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valInactive.'\''.','.'\''.$id.'\''.')">Deactivate</button>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                    
                },
            ];}?>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>
<?php Pjax::end(); ?></div>
  </div>
</div>