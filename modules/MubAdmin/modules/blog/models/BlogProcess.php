<?php 

namespace app\modules\MubAdmin\modules\blog\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\ImageUploader;
use app\models\Post;
use app\models\MubTag;
use app\models\MubUser;
use app\models\MubCategory;
use app\models\PostCategory;
use app\models\PostComment;
use app\models\PostDetail;
use app\models\PostTags;
use app\models\PostImages;
use app\models\User;
use app\models\MubUserImages;
use app\models\MubPostImages;

class BlogProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $post = new Post();
        $postDetail = new PostDetail();
        $postImages = new PostImages();
        $postCategory = new PostCategory();
    
        $this->models = [
            'post' => $post,
            'postDetail' => $postDetail,
            'postImages' => $postImages,
            'postCategory' => $postCategory
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $mubTags = new MubTag();
        $mubCategory = new MubCategory();
        $allTags = $mubTags->getAll('tag_name');
        $allCategories = $mubCategory->getAll('category_name');
        return [
            'allCategories' => $allCategories,
            'allTags' => $allTags
        ];
    }

    public function getRelatedModels($model)
    {
        $post = $model;
        $postDetail = ($model->postDetail) ? $model->postDetail : new PostDetail;
        if((!empty($model->postImages)) || (\Yii::$app->controller->action->id == 'update'))
        {
            $postImages = (isset($model->postImages[0])) ? $model->postImages[0] : new PostImages;
        }
        $postCategory = ($model->postCategory) ? $model->postCategory : new PostCategory;
        $this->relatedModels = [
            'post' => $post,
            'postDetail' => $postDetail,
            'postCategory' => $postCategory
        ];
        if((!empty($model->postImages)) || (\Yii::$app->controller->action->id == 'update'))
        {
            $this->relatedModels['postImages'] = $postImages;
        }
        return $this->relatedModels;
    }

    public function savePost($post,$postDetail)
    {
        $excerpt = strip_tags(\app\helpers\HtmlHelper::cutHtml($postDetail->post_content,250));
        $post->url = strtolower(str_replace(' ','-',$post->post_title));
        $mubUserId = User::getMubUserId();
        $post->mub_user_id = $mubUserId;
        $post->post_excerpt = $excerpt;
        return ($post->save()) ? $post->id : p($post->getErrors());
    }

    public function uploadPostImage($postImage,$post)
    {
        $postImage->url = UploadedFile::getInstance($postImage, 'url');
        if(!empty($postImage->url))
        {
            $postImage->post_id = $post->id;
            $imageUploader = new ImageUploader();
            $uploadedImage = $imageUploader->uploadImages($postImage,'url');
            $postImage->mub_user_id = $post->mub_user_id;
            $postImage->title = str_replace('/','',$postImage->url);
            $postImage->thumbnail_url = 'NA';
            $postImage->thumbnail_path = 'NA';
            $postImage->full_path = 'NA';
            $postImage->visible = '1';
            $postImage->keyword = $post->post_title;
            $postImage->description = $post->post_title;
            return ($postImage->save()) ? $postImage->id : p($postImage->getErrors());
        }
        else
        {
            return true;
        }
    }

    public function savePostDetail($postDetail,$post)
    {
        $postDetail->post_id = $post->id;
        return ($postDetail->save()) ? $postDetail->id : p($postDetail->getErrors());
    }

    public function savePostCategory($postCategory,$postId)
    {
        $postCategory->post_id = $postId;
        return ($postCategory->save()) ? $postCategory->id : p($postCategory->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['post'])&&
            isset($data['postDetail'])&&
            isset($data['postCategory'])&&
            isset($data['postImages']))
            {
            try {
                $postId = $this->savePost($data['post'],$data['postDetail']);
                    if ($postId)
                    {
                        $postImageId = $this->uploadPostImage($data['postImages'],$data['post']);
                        if($postImageId)
                        {
                            $postDetailId = $this->savePostDetail($data['postDetail'],$data['post']);
                            if($postDetailId)
                            {
                                if($postDetailId)
                                {
                                    $postCategoryId = $this->savePostCategory($data['postCategory'],$postId);
                                    if($postCategoryId)
                                    {
                                        return $postId;    
                                    }
                                }                                                     
                            }
                            else
                            {
                               p($data['postDetail']->getErrors()); 
                            }
                        }
                        else
                        {
                            p($data['postImages']->getErrors());
                        }
                    } 
                    else
                    {
                        p($data['post']->getErrors());
                    } 
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}