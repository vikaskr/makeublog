<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use app\helpers\ImageUploader;
use yii\web\JsExpression;
use yii\helpers\Url;


?>
<div id="imagePicker"></div>
<div class="post-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($post, 'post_title')->textInput(['maxlength'=> 250]) ?>

    <div class="row">
        <div class="col-md-6">
        <?= $form->field($postCategory, 'category_id')->dropDownList($allCategories, ['prompt' => 'Select A Category']) ?>
        </div>
        <div class="col-md-6">
        <?= $form->field($post, 'post_type')->dropDownList([ 'featured' => 'Featured', 'general' => 'General'],['prompt' => 'Select A Category']) ?>  
        </div> 
     </div>

    <?= $form->field($postImages, 'url')->fileInput(['accept' => 'image/*'])->label('Featured Image (Dimentions: 645 X 320)');?>

    <?php 
    if(!empty($postImages['url'])){
        $image = $postImages; ?>
        <div id="uploaded<?php echo $image->id;?>" style="display: inline; margin-right: 10px;">
        <img src="<?= '/'.ImageUploader::resizeRender($image->url, '100', '100');?>" height="100" width="100">
         </div>
    <?php }?>
    <?= $form->field($postDetail, 'post_content')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'en_GB',
    'clientOptions' => [
        //'inline' => true,
        //$content_css needs to be defined as "" or some css rules/files
        // 'content_css' => $content_css,
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "image imagetools spellchecker visualchars textcolor",
            "autosave colorpicker hr nonbreaking template"
        ],
        'toolbar1' => "undo redo | styleselect fontselect fontsizeselect forecolor backcolor | bold italic",
        'toolbar2' => "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        'image_advtab' => true,
        'templates' => [
            [ 'title'=>'Test template 1', 'content'=>'Test 1' ],
            [ 'title'=>'Test template 2', 'content'=>'Test 2' ]
        ],
        // 'visualblocks_default_state'=>true,
        'image_title' => true,
        'images_upload_url'=> '\components\uploadContentImage.php',
        // here we add custom filepicker only to Image dialog
        'file_picker_types'=>'image',
        // and here's our custom image picker
        'file_picker_callback'=> new JsExpression("function(callback, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            //If this is not included, the onchange function will not
            //be called the first time a file is chosen 
            //(at least in Chrome 58)
            var foo = document.getElementById('imagePicker');
            foo.appendChild(input);

            input.onchange = function() {
                //alert('File Input Changed');
                //console.log( this.files[0] );

                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.

                    //Remove the first period and any thing after it 
                    var rm_ext_regex = /(\.[^.]+)+/;
                    var fname = file.name;
                    fname = fname.replace( rm_ext_regex, '');

                    //Make sure filename is benign
                    var fname_regex = /^([A-Za-z0-9])+([-_])*([A-Za-z0-9-_]*)$/;
                    if( fname_regex.test( fname ) ) {
                        var id = fname + '-' + (new Date()).getTime(); //'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var blobInfo = blobCache.create(id, file, reader.result);
                        blobCache.add(blobInfo);

                        // call the callback and populate the Title field with the file name
                        callback(blobInfo.blobUri(), { title: file.name });
                    }
                    else {
                        alert( 'Invalid file name' );
                    }
                };
                //To get get rid of file picker input
                this.parentNode.removeChild(this);
            };
            input.click();
        }")
         ]
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($post->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $post->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>