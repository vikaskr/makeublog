<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'mub_user_id',
            'post_title',
            'created_at',
            [
                'attribute' => 'url',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a('Preview Post', '',
                    ['onclick' => "window.open ('".Url::toRoute(["/blog/post-detail?id=$model->url"])."'); return false", 
                            'class' => 'btn btn-warning']);
                }
            ],
            // 'post_name',
            // 'post_brief:ntext',
            // 'post_parent',
            // 'pinned',
            // 'order',
            // 'post_type',
            // 'comment_count',
            // 'approved_by',
            // 'approved_on',
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'active';
                    $id = $model->id;
                    $valInactive = 'inactive';
                    return ($model->status == 'inactive') ? '<button class="btn btn-primary" onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valActive.'\''.','.'\''.$id.'\''.')">Activate</button>' : '<button class="btn btn-danger" onClick="setModelAttribute('.'\''.$modelName.'\''.','.'\''.$attrib.'\''.','.'\''.$valInactive.'\''.','.'\''.$id.'\''.')">Deactivate</button>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                    
                },
            ],
            // 'updated_at',
            // 'del_status',

            ['header' => 'Actions','class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>