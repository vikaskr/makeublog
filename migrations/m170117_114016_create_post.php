<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_114016_create_post extends Migration
{
    public function getTableName()
    {
        return 'post';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_parent' => ['post', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id' => 'mub_user_id',
            'post_title'  => 'post_title',
            'post_excerpt' => 'post_excerpt',
            'status' => 'status',
            'pinned' => 'pinned',
            'comment_count' => 'comment_count',
            'order' => 'order',
            'created_at' => 'created_at'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'post_title' => $this->string()->notNull(),
            'post_date' => $this->integer()->defaultValue(NULL),
            'post_excerpt' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'url' => $this->string()->notNull(),
            'post_name' => $this->string(),
            'post_brief' => "text",
            'post_parent' => $this->integer()->defaultValue(NULL),
            'pinned' => "enum('0','1')",
            'order' => $this->integer()->notNull()->defaultValue('9999'),
            'post_type' => "enum('featured','general') DEFAULT 'general'",
            'comment_count' => $this->integer(),
            'approved_by' => $this->integer(),           
            'approved_on' => $this->dateTime(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
