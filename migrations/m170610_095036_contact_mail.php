<?php
namespace app\migrations;
use app\commands\Migration;

class m170610_095036_contact_mail extends Migration
{
    public function getTableName()
    {
        return 'contact_mail';
    }

    public function getKeyFields()
    {
        return [
                'name' => 'name',
                'email' => 'email'
                ];
    }

    public function getFields()
    {
        return [
            'name' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'message' => "text NOT NULL",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}

