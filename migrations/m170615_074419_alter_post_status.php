<?php

namespace app\migrations;
use app\commands\Migration;


class m170615_074419_alter_post_status extends Migration
{
    public function getTableName()
    {
        return 'post';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_parent' => ['post', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'mub_user_id' => 'mub_user_id',
            'post_title'  => 'post_title',
            'post_excerpt' => 'post_excerpt',
            'status' => 'status',
            'pinned' => 'pinned',
            'comment_count' => 'comment_count',
            'order' => 'order',
            'created_at' => 'created_at'
        ];
    }
    public function getFields()
    {
        return[];
    }
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
       
        $file = __DIR__.'/alter.sql';
        if(file_exists($file)){
          $sql = file_get_contents($file);
          $this->execute($sql);    
        }
        else{
            p($file);
        }
    }


    public function safeDown()
    {
    }
    
}
