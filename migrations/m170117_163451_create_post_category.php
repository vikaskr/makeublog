<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_163451_create_post_category extends Migration
{
    public function getTableName()
    {
        return 'post_category';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_id' => ['post', 'id'],
            'category_id' => ['mub_category','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'post_id' => 'post_id',
            'category_id'  =>  'category_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer(11)->defaultValue(NULL),
            'category_id' => $this->integer(11)->defaultValue(NULL),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
