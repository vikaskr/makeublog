<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144753_create_page_seo extends Migration
{
    public function getTableName()
    {
        return 'page_seo';
    }
    public function getForeignKeyFields()
    {
        return [
            'page_id' => ['mub_user_page', 'id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'page_id' => 'page_id',
            'keyword_text'  =>  'keyword_text',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->defaultValue(NULL),
            'keyword_text' => $this->string(255)->notNull()->defaultValue('makeubig'),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
